/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Paul
 */
import ca.odell.glazedlists.gui.TableFormat;

/**
 *
 * @author Paul
 */
public class selectProjectTableFormat implements TableFormat<Project> {

    /**
     *
     * @return the amount of columns
     */
    public int getColumnCount() {
        return 3;//set number of cols for table
    }
    /**
     *
     * @param column
     * @return the string of the column name
     */
    public String getColumnName(int column) {
        if(column == 0)      return "Project Number";//set table headings
        else if(column == 1) return "Project Title";
        else if(column == 2) return "Deadline";

        throw new IllegalStateException();
    }
    /**
     *
     * @param project
     * @param column
     * @return the object for the specified column
     */
    public Object getColumnValue(Project project, int column) {
        if(column == 0)      return project.getProjectNo();//populate table with project data
        else if(column == 1) return project.getTitle();
        else if(column == 2) return project.getDeadline();

        throw new IllegalStateException();
    }

}