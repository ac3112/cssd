/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Adam
 */
public class Project {
    private Team projectTeam;
    private String title;
    private User TeamLeader; //set up variables
    private String deadline;
    private int projectNo;
//setup getters and setters
    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the TeamLeader
     */
    public User getTeamLeader() {
        return TeamLeader;
    }

    /**
     * @param TeamLeader the TeamLeader to set
     */
    public void setTeamLeader(User TeamLeader) {
        this.TeamLeader = TeamLeader;
    }
    

    /**
     * @return the projectTeam
     */
    public Team getProjectTeam() {
        return projectTeam;
    }

    /**
     * @param projectTeam the projectTeam to set
     */
    public void setProjectTeam(Team projectTeam) {
        this.projectTeam = projectTeam;
    }

    /**
     * @return the deadline
     */
    public String getDeadline() {
        return deadline;
    }

    /**
     * @param deadline the deadline to set
     */
    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    /**
     * @return the projectNo
     */
    public int getProjectNo() {
        return projectNo;
    }

    /**
     * @param projectNo the projectNo to set
     */
    public void setProjectNo(int projectNo) {
        this.projectNo = projectNo;
    }
}
