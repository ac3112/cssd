

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author b0013120
 */
public class User {
    private String username;//global variables
    private String password; //declare variables
    private String role;
    private boolean available;
    private int userNo;
    private int currentProject;


    /**
     * @return the username
     */
    //getters and setters for user class
    
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the role
     */
    public String getRole() {
        return role;
    }

    /**
     * @param role the role to set
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     * @return the available
     */
    public boolean isAvailable() {
        return available;
    }

    /**
     * @param available the available to set
     */
    public void setAvailable(boolean available) {
        this.available = available;
    }

    /**
     * @return the userNo
     */
    public int getUserNo() {
        return userNo;
    }

    /**
     * @param userNo the userNo to set
     */
    public void setUserNo(int userNo) {
        this.userNo = userNo;
    }

    /**
     * @return the task
     */
   // public Task[] getTask() {
     //   return task;
   // }

    /**
     * @param task the task to set
     */
   // public void setTask(Task[] task) {
   //     this.task = task;
  //  }

    /**
     * @return the projects
     */
   // public Project[] getProjects() {
   //     return projects;
   // }

    /**
     */
   // public void setProjects(Project[] projects) {
   //     this.projects = projects;
   // }
    
    public String toString(){
        return username;
    }

    /**
     * @return the currentProject
     */
    public int getCurrentProject() {
        return currentProject;
    }

    /**
     * @param currentProject the currentProject to set
     */
    public void setCurrentProject(int currentProject) {
        this.currentProject = currentProject;
    }
}
