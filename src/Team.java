
import java.util.Vector;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Adam
 */
//getters and setters for team class
public class Team {
    private Vector<User> teamList = new Vector<User>();//new vector of team list

    /**
     * @return the teamList
     */
    public Vector<User> getTeamList() {
        return teamList;
    }

    /**
     * @param teamList the teamList to set
     */
    public void setTeamList(Vector<User> teamList) {
        this.teamList = teamList;
    }
    
    /**
     *
     * @param user
     */
    public void addTeamMember(User user) {
        this.teamList.add(user);
    }
    
    /**
     *
     * @param userNo
     * @return
     */
    public User getTeamMember(int userNo)
    {
        User user = new User();
        
        for (int i = 0; i > teamList.size(); i++) 
        {
            if (teamList.get(i).getUserNo() == userNo)
            {
                user = teamList.get(i);
            }
        }
        return user;//return team member based on usernumber from db
    }
    
    /**
     *
     * @param userNo
     */
    public void removeTeamMember(int userNo)
    {
        for (int i = 0; i > teamList.size(); i++)
        {
            if (teamList.get(i).getUserNo() == userNo)
            {
                teamList.remove(i);//removing team member from list based on user number from the db
            }
        }
    }
    
    /**
     *
     * @return
     */
    public int getAmountOfMembers(){
        return teamList.size();//return size of teamlist
    }
}
