
import ca.odell.glazedlists.gui.TableFormat;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Paul
 */
public class viewAssetTableFormat implements TableFormat<Asset>  {
    /**
     *
     * @return the amount of columns
     */
    public int getColumnCount() {
        return 5;//set number of cols for table
    }
    /**
     *
     * @param column
     * @return the string of the specified column name
     */
    public String getColumnName(int column) {
        if(column == 0)      return "Asset Number";//set headings for table
        else if(column == 1) return "Asset Type";
        else if(column == 2) return "Status";
        else if(column == 3) return "Description";
        else if(column == 4) return "External";

        throw new IllegalStateException();
    }
    /**
     *
     * @param asset
     * @param column
     * @return the object of the specified column
     */
    public Object getColumnValue(Asset asset, int column) {
        if(column == 0)      return asset.getAssetNo();//populate table with asset data
        else if(column == 1) return asset.getAssetType();
        else if(column == 2) return asset.getStatus();
        else if(column == 3) return asset.getDescription();
        else if(column == 4) return asset.getIsExternalString();

        throw new IllegalStateException();
    }       
}
