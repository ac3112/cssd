/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Paul
 */
import ca.odell.glazedlists.gui.TableFormat;

/**
 *
 * @author Paul
 */
public class projectTableFormat implements TableFormat<Project> {

    /**
     *
     * @return the amount of columns
     */
    public int getColumnCount() {
        return 5;//set number of cols for table
    }
    /**
     *
     * @param column
     * @return the string of the specified column
     */
    public String getColumnName(int column) {
        if(column == 0)      return "Project Number";//set table headings
        else if(column == 1) return "Project Title";
        else if(column == 2) return "Deadline";
        else if(column == 3) return "Project Manager Number";
        else if(column == 4) return "Project Manager";

        throw new IllegalStateException();
    }
    /**
     *
     * @param project
     * @param column
     * @return the object of the specified column
     */
    public Object getColumnValue(Project project, int column) {
        if(column == 0)      return project.getProjectNo();//populate table with project data
        else if(column == 1) return project.getTitle();
        else if(column == 2) return project.getDeadline();
        else if(column == 3) return project.getTeamLeader().getUserNo();
        else if(column == 4) return project.getTeamLeader().getUsername();

        throw new IllegalStateException();
    }

}
