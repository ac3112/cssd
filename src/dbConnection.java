
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//Whole class - Adam - Persistence
/**
 *
 * @author Adam
 */
public class dbConnection {
    
    String dbUser = "cssd";//set database credentials
    String dbPass = "cssd";
    String url = "jdbc:sqlserver://localhost\\SQLEXPRESS;databaseName=CSSD";
    Connection conn;

    /**
     *
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public dbConnection() throws SQLException, ClassNotFoundException {
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver"); //initalise db connection
        this.conn = DriverManager.getConnection(url, dbUser, dbPass);
    }
    
    /**
     *
     * @param query
     * @return the result based on the query string passed in
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public ResultSet getResultSet(String query) throws ClassNotFoundException, SQLException{
        
            ResultSet rs = null;
            PreparedStatement ps = conn.prepareStatement(query);//return result set based on the string passed in
            rs = ps.executeQuery();
        
        return rs;
    }
    
    /**
     *
     * @param query
     * @throws SQLException
     */
    //set database 
    public void updateDb(String query) throws SQLException{
        
        PreparedStatement ps = conn.prepareStatement(query);
        ps.executeUpdate(); //update db based on string passsed in
        
    }
    
    /**
     *
     * @param ps
     * @throws SQLException
     */
    public void insertDb(PreparedStatement ps) throws SQLException{
        ps.executeUpdate(); //insert into db based on prepared statement passed in
    }
    
    /**
     *
     * @return the db connection
     */
    public Connection returnConn(){
        return conn; //return the db connection
    }
    
    /**
     *
     * @param userno
     * @return specified user
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public User getUser(int userno) throws ClassNotFoundException, SQLException
    {
        User user = new User();//procedure to get user from database
        
            dbConnection db = new dbConnection();
            ResultSet rs = db.getResultSet("select * from users where userno=" + userno + "");
            rs.next();
            user.setUsername(rs.getString("username"));
            user.setUserNo(rs.getInt("userno"));
            user.setRole(rs.getString("role"));//setup the user class
            user.setCurrentProject(rs.getInt("currentProject"));
            user.setAvailable(rs.getBoolean("available"));

        return user;
    }
    
    /**
     *
     * @param projectno
     * @return the specified project
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public Project getProject(int projectno) throws ClassNotFoundException, SQLException
    {
        Project project = new Project();//procedure to get project from database
        
        dbConnection db = new dbConnection();
        ResultSet rs = db.getResultSet("SELECT * FROM projects WHERE projectNo=" + projectno + "");
        rs.next();
        project.setTeamLeader(this.getUser(rs.getInt("teamLeaderNo"))); //set project details
        project.setTitle(rs.getString("title"));
        project.setDeadline(rs.getString("deadline"));
        project.setProjectNo(projectno);
        
        ResultSet tl = db.getResultSet("SELECT * FROM userAllocation WHERE projectno =" + projectno + "");
        
        Team teamList = new Team();
        while(tl.next())
        {
            User user = new User();
            user = this.getUser(tl.getInt("userno")); //setup the project team
            teamList.addTeamMember(user);
        }   
        project.setProjectTeam(teamList);

        return project;//return the project
    }
    
    

    
}
