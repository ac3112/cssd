
import ca.odell.glazedlists.gui.TableFormat;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Paul
 */
class taskTableFormat implements TableFormat<Task> {

    public int getColumnCount() {
        return 11;//set number of cols for table
    }
    
    public String getColumnName(int column) {
        if(column == 0)      return "Task No";//set table headings
        else if(column == 1) return "Assigned User Number";
        else if(column == 2) return "Project Number";
        else if(column == 3) return "Title";
        else if(column == 4) return "Status";
        else if(column == 5) return "Task Type";
        else if(column == 6) return "Description";
        else if(column == 7) return "Deadline";
        else if(column == 8) return "Has Assets";
        else if(column == 9) return "Asset Number";
        else if(column == 10) return "Last Updated";

        throw new IllegalStateException();
    }
    
    public Object getColumnValue(Task task, int column) {
        if(column == 0)      return task.getTaskNo();//populate table data wiith task data
        else if(column == 1) return task.getUserNo();
        else if(column == 2) return task.getProjectNo();
        else if(column == 3) return task.getTitle();
        else if(column == 4) return task.getStatus();
        else if(column == 5) return task.getTaskType();
        else if(column == 6) return task.getDescription();
        else if(column == 7) return task.getDeadline();
        else if(column == 8) return task.getHasAssets();
        else if(column == 9) return task.getAssetNo();
        else if(column == 10) return task.getLastUpdated();

        throw new IllegalStateException();
    }

    
}
