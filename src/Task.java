
import java.sql.Date; //declare import classes

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Paul
 */
public class Task {
    
    private int taskNo;
    private int userNo;
    private int projectNo;
    private String title;
    private String description;
    private Date deadline;
    private Boolean hasAssets;
    private Date lastUpdated; //setup variables
    private String taskType;
    private String status;
    private int assetNo;
    private String username;
//setup the getters and setters
    /**
     * @return the taskNo
     */
    public int getTaskNo() {
        return taskNo;
    }

    /**
     * @param taskNo the taskNo to set
     */
    public void setTaskNo(int taskNo) {
        this.taskNo = taskNo;
    }

    /**
     * @return the userNo
     */
    public int getUserNo() {
        return userNo;
    }

    /**
     * @param userNo the userNo to set
     */
    public void setUserNo(int userNo) {
        this.userNo = userNo;
    }

    /**
     * @return the projectNo
     */
    public int getProjectNo() {
        return projectNo;
    }

    /**
     * @param projectNo the projectNo to set
     */
    public void setProjectNo(int projectNo) {
        this.projectNo = projectNo;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the deadline
     */
    public Date getDeadline() {
        return deadline;
    }

    /**
     * @param deadline the deadline to set
     */
    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    /**
     * @return the hasAssets
     */
    public Boolean getHasAssets() {
        return hasAssets;
    }

    /**
     * @param hasAssets the hasAssets to set
     */
    public void setHasAssets(Boolean hasAssets) {
        this.hasAssets = hasAssets;
    }

    /**
     * @return the lastUpdated
     */
    public Date getLastUpdated() {
        return lastUpdated;
    }

    /**
     * @param lastUpdated the lastUpdated to set
     */
    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    /**
     * @return the taskType
     */
    public String getTaskType() {
        return taskType;
    }

    /**
     * @param taskType the taskType to set
     */
    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    /**
     * @return the assetNo
     */
    public int getAssetNo() {
        return assetNo;
    }

    /**
     * @param assetNo the assetNo to set
     */
    public void setAssetNo(int assetNo) {
        this.assetNo = assetNo;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }
    
}
