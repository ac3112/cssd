/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Paul
 */
public class Asset {
    private int assetNo;
    private int projectNo;
    private String assetType;
    private String status; // Declare variables
    private String description;
    private Boolean isExternal;
    private Boolean selected = false;
    
    // Getters and Setters below.

    /**
     * @return the assetNo
     */
    public int getAssetNo() {
        return assetNo;
    }
    /**
     * @param assetNo the assetNo to set
     */
    public void setAssetNo(int assetNo) {
        this.assetNo = assetNo;
    }

    /**
     * @return the projectNo
     */
    public int getProjectNo() {
        return projectNo;
    }

    /**
     * @param projectNo the projectNo to set
     */
    public void setProjectNo(int projectNo) {
        this.projectNo = projectNo;
    }

    /**
     * @return the assetType
     */
    public String getAssetType() {
        return assetType;
    }

    /**
     * @param assetType the assetType to set
     */
    public void setAssetType(String assetType) {
        this.assetType = assetType;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the isExternal
     */
    public Boolean getIsExternal() {
        return isExternal;
    }

    /**
     * @param isExternal the isExternal to set
     */
    public void setIsExternal(Boolean isExternal) {
        this.isExternal = isExternal;
    }
    
    /**
     *
     * @return
     */
    public String getIsExternalString() {
        if (isExternal == true){
            return "Yes";
        } else
        {
            return "No";
        }
    }

    /**
     * @return the selected
     */
    public Boolean getSelected() {
        return selected;
    }

    /**
     * @param selected the selected to set
     */
    public void setSelected(Boolean selected) {
        this.selected = selected;
    }
    
    
    
}
