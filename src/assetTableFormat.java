
import ca.odell.glazedlists.gui.AdvancedTableFormat;
import ca.odell.glazedlists.gui.TableFormat;
import ca.odell.glazedlists.gui.WritableTableFormat;
import java.util.Comparator;
import javax.swing.JCheckBox;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Paul
 */
public class assetTableFormat implements AdvancedTableFormat<Asset>, WritableTableFormat<Asset> {
    
    /**
     *
     * @return the amount of columns
     */
    public int getColumnCount() {
        return 6;//set amount of cols for table
    }
    /**
     *
     * @param column
     * @return the string of the specified column
     */
    public String getColumnName(int column) {
        if(column == 0)      return "Asset Number";//set column headings
        else if(column == 1) return "Asset Type";
        else if(column == 2) return "Status";
        else if(column == 3) return "Description";
        else if(column == 4) return "External";
        else if(column == 5) return "Select?";

        throw new IllegalStateException();
    }
    /**
     *
     * @param asset
     * @param column
     * @return the object of the specified column
     */
    public Object getColumnValue(Asset asset, int column) {
        if(column == 0)      return asset.getAssetNo();//populate table with asset data
        else if(column == 1) return asset.getAssetType();
        else if(column == 2) return asset.getStatus();
        else if(column == 3) return asset.getDescription();
        else if(column == 4) return asset.getIsExternalString();
        else if(column == 5) return asset.getSelected();

        throw new IllegalStateException();
    }       

    /**
     *
     * @param i
     * @return the column class
     */
    @Override
    public Class getColumnClass(int i) {
        if (i == 5)
        {
            return Boolean.class;
        }
        else
        {
            return Asset.class;  
        }
        
    }
    /**
     *
     * @param asset
     * @param column
     * @return if the column is editable
     */
    @Override
    public boolean isEditable(Asset asset, int column) {
        if (column == 5)
        {
            return true;
        } 
        else
        {
            return false;
        }
    }
    
    /**
     *
     * @param baseObject
     * @param editedValue
     * @param column
     * @return the base object
     */
    public Asset setColumnValue(Asset baseObject, Object editedValue, int column) {
        if (column == 0) baseObject.setAssetNo((Integer) editedValue);
        else if (column == 1) baseObject.setAssetType(editedValue.toString());
        else if (column == 2) baseObject.setStatus(editedValue.toString());
        else if (column == 3) baseObject.setDescription(editedValue.toString());
        else if (column == 4) baseObject.setIsExternal((Boolean) editedValue);
        else if (column == 5) baseObject.setSelected((Boolean) editedValue);
        
        return baseObject;
    }

    /**
     *
     * @param i
     * @return the column comparator
     */
    @Override
    public Comparator getColumnComparator(int i) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}
